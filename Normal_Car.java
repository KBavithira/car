package bcas.ap.Car;

public interface Normal_Car extends Car {
	public boolean isRear_Door_Child_Lock();
	public boolean isDay_and_Night_IRVM();
	public boolean isMusic_System_With_USB_AUX_Radio_Bluetooth();
	public boolean isFull_Wheel_Covers();
	public boolean isSeatbelt_With_Pretensioners_And_Load_Limiters();
	public boolean isAir_Conditioning();

}
