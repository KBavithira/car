package bcas.ap.Car;

public class Audi_Q8 implements Luxary_Car {

	@Override
	public boolean isRear_Door_Child_Lock() {
		return true;
	}

	@Override
	public boolean isDay_and_Night_IRVM() {
		return true;
	}

	@Override
	public boolean isMusic_System_With_USB_AUX_Radio_Bluetooth() {
		return true;
	}

	@Override
	public boolean isFull_Wheel_Covers() {
		return true;
	}

	@Override
	public boolean isSeatbelt_With_Pretensioners_And_Load_Limiters() {
		return true;
	}

	@Override
	public boolean isAir_Conditioning() {
		return true;
	}

	@Override
	public String getCar_Type() {
		return "Luxury car";
	}

	@Override
	public String getCar_Brand() {
		return "Audi";
	}

	@Override
	public String getCar_Model() {
		return "Audi Q8";
	}

	@Override
	public String getCar_Color() {
		return "black";
	}

	@Override
	public String getCar_Price() {
		return "₹ 99.02 Lakh";
	}

	@Override
	public int getNo_of_seat() {
		return 5;
	}

	@Override
	public String getCar_Speed() {
		return " 155 mph";
	}

	@Override
	public boolean isDual_Airbags() {
		return true;
	}

	@Override
	public boolean isABS_System() {
		return true;
	}

	@Override
	public boolean isISOFIX_Anchorages() {
		return true;
	}

	@Override
	public boolean isPower_Steering_With_Tilt_Adjust() {
		return false;
	}

	@Override
	public boolean isPower_Windows() {
		return true;
	}

	@Override
	public boolean isCentral_Door_Locking() {
		return true;
	}
	

}
