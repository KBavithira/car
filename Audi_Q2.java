package bcas.ap.Car;

public class Audi_Q2 implements Luxary_Car {

	@Override
	public boolean isRear_Door_Child_Lock() {
		return true;
	}

	@Override
	public boolean isDay_and_Night_IRVM() {
		return true;
	}

	@Override
	public boolean isMusic_System_With_USB_AUX_Radio_Bluetooth() {
		return true;
	}

	@Override
	public boolean isFull_Wheel_Covers() {
		return true;
	}

	@Override
	public boolean isSeatbelt_With_Pretensioners_And_Load_Limiters() {
		return true;
	}

	@Override
	public boolean isAir_Conditioning() {
		return true;
	}

	@Override
	public String getCar_Type() {
		return "Luxury car";
	}

	@Override
	public String getCar_Brand() {
		return "Audi";
	}

	@Override
	public String getCar_Model() {
		return "Audi Q2";
	}

	@Override
	public String getCar_Color() {
		return "Silver";
	}

	@Override
	public String getCar_Price() {
		return "Rs. 34.99 lakh";
	}

	@Override
	public int getNo_of_seat() {
		return 5;
	}

	@Override
	public String getCar_Speed() {
		return "11.2";
	}

	@Override
	public boolean isDual_Airbags() {
         return true;
	}

	@Override
	public boolean isABS_System() {
		return true;
	}

	@Override
	public boolean isISOFIX_Anchorages() {
		return false;
	}

	@Override
	public boolean isPower_Steering_With_Tilt_Adjust() {
		return true;
	}

	@Override
	public boolean isPower_Windows() {
		return false;
	}

	@Override
	public boolean isCentral_Door_Locking() {

return true;
	}

}
