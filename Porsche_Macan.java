package bcas.ap.Car;

public class Porsche_Macan implements Luxary_Car {

	@Override
	public boolean isRear_Door_Child_Lock() {
		return true;
	}

	@Override
	public boolean isDay_and_Night_IRVM() {
		return true;
	}

	@Override
	public boolean isMusic_System_With_USB_AUX_Radio_Bluetooth() {
		return true;
	}

	@Override
	public boolean isFull_Wheel_Covers() {
		return true;
	}

	@Override
	public boolean isSeatbelt_With_Pretensioners_And_Load_Limiters() {
		return true;
	}

	@Override
	public boolean isAir_Conditioning() {
        return true;
	}

	@Override
	public String getCar_Type() {
        return "Luxury Car";
	}

	@Override
	public String getCar_Brand() {
        return "Porsche";
	}

	@Override
	public String getCar_Model() {
        return "Porsche Macan";
	}

	@Override
	public String getCar_Color() {
		return "Black";
	}

	@Override
	public String getCar_Price() {
		return "LKR21.56 Million";
	}

	@Override
	public int getNo_of_seat() {
		return 6;
	}

	@Override
	public String getCar_Speed() {
		return "6.7sec";
	}

	@Override
	public boolean isDual_Airbags() {
		return true;
	}

	@Override
	public boolean isABS_System() {
		return true;
	}

	@Override
	public boolean isISOFIX_Anchorages() {
		return true;
	}

	@Override
	public boolean isPower_Steering_With_Tilt_Adjust() {
		return true;
	}

	@Override
	public boolean isPower_Windows() {
		return true;
	}

	@Override
	public boolean isCentral_Door_Locking() {
		return true;
	}

}

