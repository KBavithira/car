package bcas.ap.Car;

public interface Car {
	public String getCar_Type();
	public String getCar_Brand();
	public String getCar_Model();
	public String getCar_Color();
	public String getCar_Price();
	public int getNo_of_seat();
	public String getCar_Speed();

}
