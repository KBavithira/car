package bcas.ap.Car;

public interface Luxary_Car extends Normal_Car {
	public boolean isDual_Airbags();
	public boolean isABS_System();
	public boolean isISOFIX_Anchorages();
	public boolean isPower_Steering_With_Tilt_Adjust();
	public boolean isPower_Windows();
	public boolean isCentral_Door_Locking();

}
