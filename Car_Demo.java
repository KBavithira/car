package bcas.ap.Car;

public class Car_Demo {
	public static void main(String[] args) {
		
		
		BMW_2_Series car1= new BMW_2_Series();
		System.out.println("The car Type is: "+ car1.getCar_Type() +"\n" + "The car Brand: "+ car1.getCar_Brand() +"\n" + 
		"The car model : "+ car1.getCar_Model() +"\n" + "The car colour  : "+ car1.getCar_Color() +"\n" + 
	    "The car price : "+ car1.getCar_Price() +"\n" +	"The car speed : "+ car1.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car1.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car1.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car1.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car1.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car1.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car1.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car1.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car1.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car1.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car1.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car1.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car1.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car1.isCentral_Door_Locking()  +"\n" );
		
		
		BMW_Z4 car2= new BMW_Z4();
		System.out.println("The car Type is: "+ car2.getCar_Type() +"\n" + "The car Brand: "+ car2.getCar_Brand() +"\n" + 
		"The car model : "+ car2.getCar_Model() +"\n" + "The car colour  : "+ car2.getCar_Color() +"\n" + 
	    "The car price : "+ car2.getCar_Price() +"\n" +	"The car speed : "+ car2.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car2.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car2.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car2.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car2.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car2.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car2.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car2.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car2.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car2.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car2.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car2.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car2.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car2.isCentral_Door_Locking()  +"\n" );
		
		
		BMW_X3 car3= new BMW_X3();
		System.out.println("The car Type is: "+ car3.getCar_Type() +"\n" + "The car Brand: "+ car3.getCar_Brand() +"\n" + 
		"The car model : "+ car3.getCar_Model() +"\n" + "The car colour  : "+ car3.getCar_Color() +"\n" + 
	    "The car price : "+ car3.getCar_Price() +"\n" +	"The car speed : "+ car3.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car3.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car3.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car3.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car3.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car3.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car3.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car3.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car3.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car3.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car3.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car3.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car3.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car3.isCentral_Door_Locking()  +"\n" );
		
		
		Audi_Q2 car4= new Audi_Q2();
		System.out.println("The car Type is: "+ car4.getCar_Type() +"\n" + "The car Brand: "+ car4.getCar_Brand() +"\n" + 
		"The car model : "+ car4.getCar_Model() +"\n" + "The car colour  : "+ car4.getCar_Color() +"\n" + 
	    "The car price : "+ car4.getCar_Price() +"\n" +	"The car speed : "+ car4.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car4.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car4.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car4.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car4.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car4.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car4.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car4.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car4.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car4.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car4.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car4.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car4.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car4.isCentral_Door_Locking()  +"\n" );
		
		
		Audi_Q8 car5= new Audi_Q8();
		System.out.println("The car Type is: "+ car5.getCar_Type() +"\n" + "The car Brand: "+ car5.getCar_Brand() +"\n" + 
		"The car model : "+ car5.getCar_Model() +"\n" + "The car colour  : "+ car5.getCar_Color() +"\n" + 
	    "The car price : "+ car5.getCar_Price() +"\n" +	"The car speed : "+ car5.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car5.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car5.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car5.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car5.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car5.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car5.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car5.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car5.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car5.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car5.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car5.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car5.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car5.isCentral_Door_Locking()  +"\n" );
		
		
		Audi_RS7 car6= new Audi_RS7();
		System.out.println("The car Type is: "+ car6.getCar_Type() +"\n" + "The car Brand: "+ car6.getCar_Brand() +"\n" + 
		"The car model : "+ car6.getCar_Model() +"\n" + "The car colour  : "+ car6.getCar_Color() +"\n" + 
	    "The car price : "+ car6.getCar_Price() +"\n" +	"The car speed : "+ car6.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car6.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car6.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car6.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car6.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car6.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car6.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car6.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car6.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car6.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car6.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car6.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car6.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car6.isCentral_Door_Locking()  +"\n" );
		
		
		Porsche_Cayenne car7= new Porsche_Cayenne();
		System.out.println("The car Type is: "+ car7.getCar_Type() +"\n" + "The car Brand: "+ car7.getCar_Brand() +"\n" + 
		"The car model : "+ car7.getCar_Model() +"\n" + "The car colour  : "+ car7.getCar_Color() +"\n" + 
	    "The car price : "+ car7.getCar_Price() +"\n" +	"The car speed : "+ car7.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car7.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car7.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car7.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car7.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car7.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car7.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car7.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car7.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car7.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car7.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car7.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car7.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car7.isCentral_Door_Locking()  +"\n" );
		
		
		Porsche_Panamera car8= new Porsche_Panamera();
		System.out.println("The car Type is: "+ car8.getCar_Type() +"\n" + "The car Brand: "+ car8.getCar_Brand() +"\n" + 
		"The car model : "+ car8.getCar_Model() +"\n" + "The car colour  : "+ car8.getCar_Color() +"\n" + 
	    "The car price : "+ car8.getCar_Price() +"\n" +	"The car speed : "+ car8.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car8.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car8.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car8.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car8.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car8.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car8.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car8.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car8.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car8.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car8.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car8.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car8.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car8.isCentral_Door_Locking()  +"\n" );
		
		
		Porsche_Macan car9= new Porsche_Macan();
		System.out.println("The car Type is: "+ car9.getCar_Type() +"\n" + "The car Brand: "+ car9.getCar_Brand() +"\n" + 
		"The car model : "+ car9.getCar_Model() +"\n" + "The car colour  : "+ car9.getCar_Color() +"\n" + 
	    "The car price : "+ car9.getCar_Price() +"\n" +	"The car speed : "+ car9.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car9.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car9.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car9.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car9.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car9.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car9.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car9.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car9.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car9.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car9.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car9.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car9.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car9.isCentral_Door_Locking()  +"\n" );
		
		
		Porsche_718 car10= new Porsche_718();
		System.out.println("The car Type is: "+ car10.getCar_Type() +"\n" + "The car Brand: "+ car10.getCar_Brand() +"\n" + 
		"The car model : "+ car10.getCar_Model() +"\n" + "The car colour  : "+ car10.getCar_Color() +"\n" + 
	    "The car price : "+ car10.getCar_Price() +"\n" +	"The car speed : "+ car10.getCar_Speed() +"\n" + 
	    "No of Seats : "+ car10.getNo_of_seat() +"\n" + 	"It has Rear Door Child Lock: "+ car10.isRear_Door_Child_Lock() + "\n" +
	    "It has Day and Night IRVM: "+ car10.isDay_and_Night_IRVM () +"\n" +	"It has Music System With USB AUX Radio Bluetooth: "+ car10.isMusic_System_With_USB_AUX_Radio_Bluetooth () +"\n" +
		"It has Full Wheel Covers: "+ car10.isFull_Wheel_Covers()  +"\n" + "It has Seatbelt With Pretensioners And Load Limiters: "+ car10.isSeatbelt_With_Pretensioners_And_Load_Limiters()  +"\n" + 
		"It has Air Conditioning: "+ car10.isAir_Conditioning()  +"\n" +	"It has Dual Airbags: "+ car10.isDual_Airbags()  +"\n" +
		"It has ABS System: "+ car10.isABS_System()  +"\n" +	"It has ISOFIX Anchorages: "+ car10.isISOFIX_Anchorages()  +"\n" +
		"It has Power Steering With Tilt Adjust: "+ car10.isPower_Steering_With_Tilt_Adjust()  +"\n" +"It has Power Windows: "+ car10.isPower_Windows()  +"\n" +
		"It has Central Door Locking: "+ car10.isCentral_Door_Locking()  +"\n" );
	}

}
